package com.example.blog.core.constant;

public class CodeMsg {

    public static CodeMsg OK = new CodeMsg(200, "success");
    public static CodeMsg CREATED = new CodeMsg(201, "请求成功，新的资源已创建");
    public static CodeMsg ACCEPTED = new CodeMsg(202, "请求成功，后续进行异步处理");
    public static CodeMsg NO_CONTENT = new CodeMsg(204, "内容不存在");
    public static CodeMsg MULTIPLE_CHOICES = new CodeMsg(300, "存在多个资源");
    public static CodeMsg MOVED_PERMANENTLY = new CodeMsg(301, "资源被永久转移");
    public static CodeMsg NOT_MODIFIED = new CodeMsg(304, "自上一次访问后没有发生更新");
    public static CodeMsg TEMPORARY_REDIRECT = new CodeMsg(307, "请求的资源被暂时转移");
    public static CodeMsg BAD_REQUEST = new CodeMsg(400, "请求不正确");
    public static CodeMsg UNAUTHORIZED = new CodeMsg(401, "认证失效");
    public static CodeMsg FORBIDDEN = new CodeMsg(403, "禁止访问");
    public static CodeMsg NOT_FOUND = new CodeMsg(404, "没有找到指定资源");
    public static CodeMsg METHOD_NOT_ALLOWED = new CodeMsg(405, "无法使用指定的方法");
    public static CodeMsg NOT_ACCEPTABLE = new CodeMsg(406, "同Accept相关联的首部里含有无法处理的内容");
    public static CodeMsg REQUEST_TIMEOUT = new CodeMsg(408, "请求在规定时间内没有处理结束");
    public static CodeMsg CONFLICT = new CodeMsg(409, "资源存在冲突");
    public static CodeMsg GONE = new CodeMsg(410, "指定的资源已不存在");
    public static CodeMsg REQUEST_ENTITY_TOO_LARGE = new CodeMsg(413, "请求消息体太大");
    public static CodeMsg REQUEST_URI_TOO_LONG = new CodeMsg(414, "请求的URI太长");
    public static CodeMsg UNSUPPORTED_MEDIA_TYPE = new CodeMsg(415, "不支持所指定的媒体类型");
    public static CodeMsg TOO_MANY_REQUEST = new CodeMsg(429, "请求次数过多");
    public static CodeMsg INTERNAL_SERVER_ERROR = new CodeMsg(500, "服务端发生错误");
    public static CodeMsg SERVICE_UNAVAILABLE = new CodeMsg(503, "服务器暂时停止运行");
    public static CodeMsg SEARCH_FAIL = new CodeMsg(10000, "数据查询失败");
    public static CodeMsg ADD_FAIL = new CodeMsg(20000, "数据添加失败");
    public static CodeMsg SAVE_FAIL = new CodeMsg(30000, "数据保存失败");
    public static CodeMsg UPDATE_FAIL = new CodeMsg(40000, "数据更新失败");
    public static CodeMsg DELETE_FAIL = new CodeMsg(50000, "数据删除失败");
    private int code;
    private String msg;

    public CodeMsg() {
    }

    public CodeMsg(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return this.code;
    }

    public String getMsg() {
        return this.msg;
    }

    public String toString() {
        return "CodeMsg{code=" + this.code + ", msg='" + this.msg + '\'' + '}';
    }
}