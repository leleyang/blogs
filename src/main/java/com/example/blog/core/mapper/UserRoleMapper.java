package com.example.blog.core.mapper;

import com.example.blog.core.model.UserRole;
import tk.mybatis.mapper.common.Mapper;

public interface UserRoleMapper extends Mapper<UserRole> {
}