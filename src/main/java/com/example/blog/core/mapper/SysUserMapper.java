package com.example.blog.core.mapper;

import com.example.blog.core.model.SysUser;
import tk.mybatis.mapper.common.Mapper;

public interface SysUserMapper extends Mapper<SysUser> {
}