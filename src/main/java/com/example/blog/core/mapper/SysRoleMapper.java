package com.example.blog.core.mapper;

import com.example.blog.core.model.SysRole;
import tk.mybatis.mapper.common.Mapper;

public interface SysRoleMapper extends Mapper<SysRole> {
}