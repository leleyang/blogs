package com.example.blog.core.mapper;

import com.example.blog.core.model.RoleRight;
import tk.mybatis.mapper.common.Mapper;

public interface RoleRightMapper extends Mapper<RoleRight> {
}