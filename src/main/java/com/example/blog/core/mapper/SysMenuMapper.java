package com.example.blog.core.mapper;

import com.example.blog.core.model.SysMenu;
import tk.mybatis.mapper.common.Mapper;

public interface SysMenuMapper extends Mapper<SysMenu> {
}