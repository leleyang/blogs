package com.example.blog.core.mapper;

import com.example.blog.core.model.SysRight;
import tk.mybatis.mapper.common.Mapper;

public interface SysRightMapper extends Mapper<SysRight> {
}