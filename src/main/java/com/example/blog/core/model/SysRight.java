package com.example.blog.core.model;

import javax.persistence.*;
import java.util.List;

@Table(name = "sys_right")
public class SysRight {
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 权限名字
     */
    @Column(name = "authName")
    private String authname;

    @Column(name = "level")
    private Integer level;

    @Column(name = "pid")
    private Integer pid;

    @Column(name = "path")
    private String path;

    private List<SysRight> children;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取权限名字
     *
     * @return authName - 权限名字
     */
    public String getAuthname() {
        return authname;
    }

    /**
     * 设置权限名字
     *
     * @param authname 权限名字
     */
    public void setAuthname(String authname) {
        this.authname = authname;
    }

    /**
     * @return level
     */
    public Integer getLevel() {
        return level;
    }

    /**
     * @param level
     */
    public void setLevel(Integer level) {
        this.level = level;
    }

    /**
     * @return pid
     */
    public Integer getPid() {
        return pid;
    }

    /**
     * @param pid
     */
    public void setPid(Integer pid) {
        this.pid = pid;
    }

    /**
     * @return path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path
     */
    public void setPath(String path) {
        this.path = path;
    }

    public List<SysRight> getChildren() {
        return children;
    }

    public void setChildren(List<SysRight> children) {
        this.children = children;
    }
}