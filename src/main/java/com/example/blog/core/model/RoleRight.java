package com.example.blog.core.model;

import javax.persistence.*;

@Table(name = "role_right")
public class RoleRight {
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    @Column(name = "role_id")
    private Integer roleId;

    @Column(name = "right_id")
    private Integer rightId;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return role_id
     */
    public Integer getRoleId() {
        return roleId;
    }

    /**
     * @param roleId
     */
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    /**
     * @return right_id
     */
    public Integer getRightId() {
        return rightId;
    }

    /**
     * @param rightId
     */
    public void setRightId(Integer rightId) {
        this.rightId = rightId;
    }
}