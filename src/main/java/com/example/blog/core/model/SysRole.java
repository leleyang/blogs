package com.example.blog.core.model;

import javax.persistence.*;
import java.util.List;

@Table(name = "sys_role")
public class SysRole {
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    @Column(name = "role")
    private String role;

    @Column(name = "name")
    private String name;

    @Column(name = "modules")
    private String modules;

    private List<SysRight> children;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return role
     */
    public String getRole() {
        return role;
    }

    /**
     * @param role
     */
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return modules
     */
    public String getModules() {
        return modules;
    }

    /**
     * @param modules
     */
    public void setModules(String modules) {
        this.modules = modules;
    }


    public List<SysRight> getChildren() {
        return children;
    }

    public void setChildren(List<SysRight> children) {
        this.children = children;
    }
}