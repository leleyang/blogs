package com.example.blog.core.model.vo;



import com.example.blog.core.constant.CodeMsg;
import java.io.Serializable;
/**
 * @author yanglele 2019/11/28 13:50
 * @description: conteoller 返回对象类
 */
public class ResponseVo<T> implements Serializable {
    private static final long serialVersionUID = -635302489444039690L;
    private int code;
    private String message;
    private T data;

    public ResponseVo() {
    }

    public ResponseVo(T data) {
        this.code = CodeMsg.OK.getCode();
        this.message = CodeMsg.OK.getMsg();
        this.data = data;
    }

    public ResponseVo(CodeMsg msg) {
        if (msg != null) {
            this.code = msg.getCode();
            this.message = msg.getMsg();
        }
    }

    public ResponseVo(int code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public static <T> ResponseVo<T> success(T data) {
        return new ResponseVo(data);
    }

    public static <T> ResponseVo<T> success() {
        return success((null));
    }

    public static <T> ResponseVo<T> error(Integer code, String msg) {
        return fail(new CodeMsg(code, msg));
    }

    public static <T> ResponseVo<T> error(String msg) {
        return fail(new CodeMsg(400, msg));
    }

    public static <T> ResponseVo<T> fail(CodeMsg msg) {
        return new ResponseVo(msg);
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return this.message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String toString() {
        return "ResponseVo [code=" + this.code + ", message=" + this.message + ", data=" + this.data + "]";
    }
}

