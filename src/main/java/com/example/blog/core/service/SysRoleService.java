package com.example.blog.core.service;

import com.example.blog.core.model.SysRight;
import com.example.blog.core.model.SysRole;

import java.util.List;

public interface SysRoleService {

    List<SysRole> list();

    List<SysRight> deleteRoleRight(int roleId, int rightId);
    //删除角色下所有权限
    int deleteRoleAllRight(int roleId);

    int saveRightByRole(int roleId, int[] rightId);
}
