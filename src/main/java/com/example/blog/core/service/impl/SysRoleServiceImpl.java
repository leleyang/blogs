package com.example.blog.core.service.impl;

import com.example.blog.core.mapper.RoleRightMapper;
import com.example.blog.core.mapper.SysRightMapper;
import com.example.blog.core.mapper.SysRoleMapper;
import com.example.blog.core.model.RoleRight;
import com.example.blog.core.model.SysRight;
import com.example.blog.core.model.SysRole;
import com.example.blog.core.service.SysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SysRoleServiceImpl implements SysRoleService {

    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Autowired
    private RoleRightMapper roleRightMapper;

    @Autowired
    private SysRightMapper sysRightMapper;


    @Override
    public List<SysRole> list() {
        List<SysRole> listSysRole = sysRoleMapper.selectAll();
        RoleRight roleRight = new RoleRight();
        SysRight sysRight;
        List<SysRight> listParentRight = new ArrayList();
        List<SysRight> listChildRight = new ArrayList();
        List<SysRight> listSysRight ;
        for(SysRole sysRole: listSysRole) {
            roleRight.setRoleId(sysRole.getId());
            List<RoleRight> listRoleRight =  roleRightMapper.select(roleRight);
            for(RoleRight rr: listRoleRight) {
                sysRight = sysRightMapper.selectByPrimaryKey(rr.getRightId());
                listParentRight.add(sysRight);
            }
            sysRole.setChildren(listChildRight);

        }
        for(SysRight parentRight: listParentRight) {
            listSysRight = new ArrayList<>();
            for(SysRight childRight: listParentRight) {
                if(parentRight.getId() == childRight.getPid()){
                    listSysRight.add(childRight);
                }
                parentRight.setChildren(listSysRight);
            }
            if(parentRight.getLevel() == 0) {
                listChildRight.add(parentRight);
            }
        }

        return listSysRole;
    }

    @Override
    public List<SysRight> deleteRoleRight(int roleId, int rightId) {
        RoleRight roleRight = new RoleRight();
        roleRight.setRoleId(roleId);
        roleRight.setRightId(rightId);
        if(roleRightMapper.delete(roleRight) != 0) {
            List<SysRight> listParentRight = new ArrayList();
            List<SysRight> listChildRight = new ArrayList();
            List<SysRight> listSysRight ;
            roleRight.setRightId(null);
            List<RoleRight> listRoleRight =  roleRightMapper.select(roleRight);
            for(RoleRight rr: listRoleRight) {
                SysRight sysRight = sysRightMapper.selectByPrimaryKey(rr.getRightId());
                listParentRight.add(sysRight);
            }
            for(SysRight parentRight: listParentRight) {
                listSysRight = new ArrayList<>();
                for(SysRight childRight: listParentRight) {
                    if(parentRight.getId() == childRight.getPid()){
                        listSysRight.add(childRight);
                    }
                    parentRight.setChildren(listSysRight);
                }
                if(parentRight.getLevel() == 0) {
                    listChildRight.add(parentRight);
                }
            }
            return listChildRight;
        }
        return null;
    }

    @Override
    public int deleteRoleAllRight(int roleId) {
        RoleRight roleRight = new RoleRight();
        roleRight.setRoleId(roleId);
        return roleRightMapper.delete(roleRight);
    }

    @Override
    public int saveRightByRole(int roleId, int[] rightId) {
        RoleRight roleRight;
        for(int i = 0 ; i < rightId.length; i++){
            roleRight = new RoleRight();
            roleRight.setRoleId(roleId);
            roleRight.setRightId(rightId[i]);
            roleRightMapper.insert(roleRight);

        }

        return 0;
    }
}
