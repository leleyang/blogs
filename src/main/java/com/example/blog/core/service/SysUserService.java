package com.example.blog.core.service;

import com.example.blog.core.model.SysUser;
import com.github.pagehelper.PageInfo;


public interface SysUserService {

    int insertSysUser(SysUser sysUser);

    SysUser getSysUserByLoginName(String username);

    PageInfo getPageInfo(int pageNum, int pageSize, String query);

    int update(SysUser sysUser);

    int save(SysUser sysUser);

    SysUser get(Integer id);

    int delete(Integer id);

    SysUser saveRole(Integer id, Integer roleId);
}
