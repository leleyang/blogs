package com.example.blog.core.service.impl;


import com.example.blog.core.mapper.SysRightMapper;
import com.example.blog.core.model.SysRight;
import com.example.blog.core.service.SysRightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SysRightServiceImpl implements SysRightService {
    @Autowired
    private SysRightMapper sysRightMapper;
    @Override
    public List<SysRight> list() {
        return sysRightMapper.selectAll();
    }

    @Override
    public SysRight get(Integer id) {
        return null;
    }

    @Override
    public int save(SysRight Right) {
        return 0;
    }

    @Override
    public int delete(Integer id) {
        return 0;
    }

    @Override
    public int update(SysRight Right) {
        return 0;
    }

    @Override
    public List<SysRight> tree() {
        List<SysRight> list = sysRightMapper.selectAll();
        List<SysRight> listSysRight;
        List<SysRight> listRight = new ArrayList<>();
        for(SysRight sysRight: list) {
            listSysRight = new ArrayList<>();
            for(SysRight right: list) {

                if(sysRight.getId() == right.getPid()){
                    listSysRight.add(right);
                }
            }
            sysRight.setChildren(listSysRight);
            if(sysRight.getLevel() == 0) {
                listRight.add(sysRight);
            }
        }
        return listRight;
    }
}
