package com.example.blog.core.service.impl;

import com.example.blog.core.mapper.RoleRightMapper;
import com.example.blog.core.mapper.SysUserMapper;
import com.example.blog.core.mapper.UserRoleMapper;
import com.example.blog.core.model.SysUser;
import com.example.blog.core.model.UserRole;
import com.example.blog.core.service.SysUserService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;
import tk.mybatis.mapper.util.Sqls;
import tk.mybatis.mapper.weekend.WeekendSqls;

/**
 * @author yanglele 2019/11/29 10:15
 * @description: mapper用法示例
 */
@Service
public class SysUserServiceImpl implements SysUserService {
    @Autowired
    private SysUserMapper sysUserMapper;

    private UserRoleMapper userRoleMapper;


    @Override
    public int insertSysUser(SysUser sysUser) {
        //查询条件
        Example example = new Example(SysUser.class);
        //动态sql
        Criteria criteria = example.createCriteria();
        example.setForUpdate(true);
        example.createCriteria().andGreaterThan("id", 100).andLessThan("id",151);
        sysUserMapper.selectByExample(example);
        //排序
        Example example1 = new Example(SysUser.class);
        example.orderBy("id").desc().orderBy("countryname").orderBy("countrycode").asc();
        List<SysUser> countries = sysUserMapper.selectByExample(example);
        //Example.builder 方式
        Example example2 = Example.builder(SysUser.class)
            .select("countryname")
            .where(Sqls.custom().andGreaterThan("id", 100))
            .orderByAsc("countrycode")
            .forUpdate()
            .build();
        List<SysUser> countries1 = sysUserMapper.selectByExample(example);
        //分页
        //获取第1页，10条内容，默认查询总数count
        PageHelper.startPage(1, 10);
        //紧跟着的第一个select方法会被分页
        List<SysUser> list = sysUserMapper.selectAll();
        new PageInfo(list);
        return 0;
    }

    @Override
    public SysUser getSysUserByLoginName(String username) {
        SysUser sysUser = new SysUser();
        sysUser.setLoginName(username);
        return sysUserMapper.selectOne(sysUser);
    }

    @Override
    public PageInfo getPageInfo(int pageNum, int pageSize, String query) {
        //获取第pageNum页，pageSize条内容，默认查询总数count
        PageHelper.startPage(pageNum, pageSize);
        //紧跟着的第一个select方法会被分页
        List<SysUser> list = null;
        if(query != null){
            list = sysUserMapper.selectByExample(new Example.Builder(SysUser.class)
                    .where(WeekendSqls.<SysUser>custom().andLike(SysUser::getName,"%"+query+"%")).build());
        }else {
            list = sysUserMapper.selectAll();
        }


        return new PageInfo(list);
    }

    @Override
    public int update(SysUser sysUser) {
        return sysUserMapper.updateByPrimaryKeySelective(sysUser);
    }

    @Override
    public int save(SysUser sysUser) {
        return sysUserMapper.insert(sysUser);
    }

    @Override
    public SysUser get(Integer id) {
        return sysUserMapper.selectByPrimaryKey(id);
    }

    @Override
    public int delete(Integer id) {
        return sysUserMapper.deleteByPrimaryKey(id);
    }

    @Override
    public SysUser saveRole(Integer id, Integer roleId) {
        UserRole userRole = new UserRole();
        userRole.setRoleId(roleId);
        userRole.setUserId(id);
        userRoleMapper.insert(userRole);
        return get(id);
    }
}
