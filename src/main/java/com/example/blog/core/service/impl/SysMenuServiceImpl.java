package com.example.blog.core.service.impl;


import com.example.blog.core.mapper.SysMenuMapper;
import com.example.blog.core.model.SysMenu;
import com.example.blog.core.service.SysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
/**
 * @author yanglele 2019/12/17 14:52
 * @description:
 */
@Service
public class SysMenuServiceImpl implements SysMenuService {
    @Autowired
    private SysMenuMapper sysMenuMapper;
    @Override
    public List<SysMenu> list() {
        List<SysMenu> listMenu = sysMenuMapper.selectAll();
        List<SysMenu> listParentMenu = new ArrayList();
        List<SysMenu> listChildMenu = new ArrayList();
        List<SysMenu> listChild;
        for(SysMenu menu : listMenu) {
            if(menu.getParentId() != 0){
                listChildMenu.add(menu);
            } else {
                listParentMenu.add(menu);
            }
        }

        for(SysMenu parentMenu : listParentMenu) {
            listChild = new ArrayList<SysMenu>();
            for(SysMenu childMenu : listChildMenu) {
                if(parentMenu.getId() == childMenu.getParentId()){
                    listChild.add(childMenu);
                }
            }
            parentMenu.setChildren(listChild);
        }

        return listParentMenu;
    }

    @Override
    public SysMenu get(Integer id) {
        return null;
    }

    @Override
    public int save(SysMenu menu) {
        return 0;
    }

    @Override
    public int delete(Integer id) {
        return 0;
    }

    @Override
    public int update(SysMenu menu) {
        return 0;
    }
}
