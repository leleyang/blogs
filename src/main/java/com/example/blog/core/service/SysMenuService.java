package com.example.blog.core.service;

import com.example.blog.core.model.SysMenu;

import java.util.List;

public interface SysMenuService {

    List<SysMenu> list();

    SysMenu get(Integer id);

    int save(SysMenu menu);

    int delete(Integer id);

    int update(SysMenu menu);
}
