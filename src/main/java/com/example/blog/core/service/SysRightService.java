package com.example.blog.core.service;

import com.example.blog.core.model.SysRight;

import java.util.List;

public interface SysRightService {
    List<SysRight> list();

    SysRight get(Integer id);

    int save(SysRight Right);

    int delete(Integer id);

    int update(SysRight Right);

    List<SysRight> tree();
}
