package com.example.blog.core.controller;

import com.example.blog.core.constant.CodeMsg;
import com.example.blog.core.model.SysRight;
import com.example.blog.core.model.SysRole;
import com.example.blog.core.model.vo.ResponseVo;
import com.example.blog.core.service.SysRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/sysRole")
@CrossOrigin
@Api(value = "sysRoleController", description = "角色列表")
public class SysRoleController {

    @Autowired
    private SysRoleService sysRoleService;

    @ApiOperation(value = "获取所有角色")
    @GetMapping("/all")
    public ResponseVo<Object> allSysUser(){
        List<SysRole> list = sysRoleService.list();
        return ResponseVo.success(list);
    }

    @ApiOperation(value = "删除角色权限")
    @DeleteMapping("/{roleId}/{rightId}")
    public ResponseVo<Object> deleteUser(@PathVariable Integer roleId, @PathVariable Integer rightId){
        List<SysRight> listSysRight = sysRoleService.deleteRoleRight(roleId, rightId);
        if(listSysRight != null) {
            return ResponseVo.success(listSysRight);
        }
        return ResponseVo.fail(CodeMsg.NO_CONTENT);
    }
    @ApiOperation(value = "分配角色权限")
    @PostMapping("/distributionRight")
    public ResponseVo<Object> distributionRight(Integer roleId, int[] rightIds){
        sysRoleService.deleteRoleAllRight(roleId);
        sysRoleService.saveRightByRole(roleId, rightIds);
        return ResponseVo.success("更新成功");
    }

}
