package com.example.blog.core.controller;

import com.example.blog.core.constant.CodeMsg;
import com.example.blog.core.model.SysMenu;
import com.example.blog.core.model.vo.ResponseVo;
import com.example.blog.core.service.SysMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @author yanglele 2019/11/28 13:49
 * @description:
 */
@RestController
@RequestMapping("/sysMenu")
@CrossOrigin
@Api(value = "sysMenuController", description = "系统菜单")
public class SysMenuController {
    @Autowired
    private SysMenuService sysMenuService;


    @ApiOperation(value = "获取按钮")
    @GetMapping("/all")
    public ResponseVo<Object> allSysMenu(){
        List<SysMenu> listMenu = sysMenuService.list();
        return ResponseVo.success(listMenu);
    }

    @GetMapping(value = "/{id}")
    public ResponseVo<Object> get(@PathVariable Integer id){
        SysMenu menu = sysMenuService.get(id);
        if(menu == null) {
            return ResponseVo.fail(CodeMsg.NO_CONTENT);
        }
        return ResponseVo.success(menu);
    }

    @PostMapping(value = "/")
    public ResponseVo<Object> add(@RequestBody SysMenu sysMenu){
        if(sysMenu.getId() != null) {
            return ResponseVo.fail(CodeMsg.NO_CONTENT);
        }
        return null;
    }

    @DeleteMapping(value = "/{id}")
    public ResponseVo<Object> delete(@PathVariable String id) {
        if (id == null) {
            return ResponseVo.fail(CodeMsg.NO_CONTENT);
        }
        return null;
    }

    @PutMapping(value = "/")
    public ResponseVo<Object> update(@RequestBody SysMenu sysMenu) {
        if(sysMenu.getId() == null) {
            return ResponseVo.fail(CodeMsg.NO_CONTENT);
        }
        return null;
    }
}
