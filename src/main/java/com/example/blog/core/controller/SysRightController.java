package com.example.blog.core.controller;

import com.example.blog.core.constant.CodeMsg;
import com.example.blog.core.model.SysRight;
import com.example.blog.core.model.vo.ResponseVo;
import com.example.blog.core.service.SysRightService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * @author yanglele 2019/11/28 13:49
 * @description:
 */
@RestController
@RequestMapping("/sysRight")
@CrossOrigin
@Api(value = "sysRoleController", description = "角色列表")
public class SysRightController {
    @Autowired
    private SysRightService sysRightService;


    @ApiOperation(value = "获取权限列表")
    @GetMapping("/all")
    public ResponseVo<Object> allSysRight(){
        List<SysRight> listRight = sysRightService.list();
        return ResponseVo.success(listRight);
    }

    @ApiOperation(value = "获取树状权限列表")
    @GetMapping("/tree")
    public ResponseVo<Object> treeSysRight(){
        List<SysRight> listRight = sysRightService.tree();
        return ResponseVo.success(listRight);
    }

    @GetMapping(value = "/{id}")
    public ResponseVo<Object> get(@PathVariable Integer id){
        SysRight Right = sysRightService.get(id);
        if(Right == null) {
            return ResponseVo.fail(CodeMsg.NO_CONTENT);
        }
        return ResponseVo.success(Right);
    }

    @PostMapping(value = "/")
    public ResponseVo<Object> add(@RequestBody SysRight sysRight){
        if(sysRight.getId() != null) {
            return ResponseVo.fail(CodeMsg.NO_CONTENT);
        }
        return null;
    }

    @DeleteMapping(value = "/{id}")
    public ResponseVo<Object> delete(@PathVariable String id) {
        if (id == null) {
            return ResponseVo.fail(CodeMsg.NO_CONTENT);
        }
        return null;
    }

    @PutMapping(value = "/")
    public ResponseVo<Object> update(@RequestBody SysRight sysRight) {
        if(sysRight.getId() == null) {
            return ResponseVo.fail(CodeMsg.NO_CONTENT);
        }
        return null;
    }
}
