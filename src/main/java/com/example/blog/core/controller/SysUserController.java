package com.example.blog.core.controller;

import com.example.blog.core.constant.CodeMsg;
import com.example.blog.core.model.SysUser;
import com.example.blog.core.model.vo.ResponseVo;
import com.example.blog.core.service.SysUserService;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * @author yanglele 2019/11/28 13:49
 * @description:
 */
@RestController
@RequestMapping("/sysUser")
@CrossOrigin
@Api(value = "sysUserController", description = "系统用户信息")
public class SysUserController {
    @Autowired
    private SysUserService sysUserService;

    @ApiOperation(value = "用户登陆接口")
    @PostMapping("/login")
    public ResponseVo<Object> login(String username, String password){
        if(username == null || password == null){
            return ResponseVo.fail(CodeMsg.BAD_REQUEST);
        }
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(username, password);
        try{
            subject.login(token);
        }catch (UnknownAccountException e){
            return ResponseVo.fail(CodeMsg.UNAUTHORIZED);
        }catch (IncorrectCredentialsException e){
            return ResponseVo.fail(CodeMsg.UNAUTHORIZED);
        }
        return ResponseVo.success();

    }
    @ApiOperation(value = "获取所有用户")
    @GetMapping("/all")
    public ResponseVo<Object> allSysUser(int pageNum, int pageSize, String query){
        PageInfo PageInfo = sysUserService.getPageInfo(pageNum, pageSize, query);
        return ResponseVo.success(PageInfo);
    }
    @ApiOperation(value = "修改用户状态")
    @PutMapping("/updStatus")
    public ResponseVo<Object> updStatus(@RequestBody SysUser sysUser){
        if(sysUserService.update(sysUser) != 0) {
            return ResponseVo.success("修改用户状态成功");
        }else {
            return ResponseVo.fail(CodeMsg.NO_CONTENT);
        }
    }

    @ApiOperation(value = "增加用户")
    @PostMapping("/addUser")
    public ResponseVo<Object> addUser(@RequestBody SysUser sysUser, String name){
        if(sysUserService.save(sysUser) != 0) {
            return ResponseVo.success("增加用户成功");
        }else {
            return ResponseVo.fail(CodeMsg.NO_CONTENT);
        }
    }

    @ApiOperation(value = "查询用户")
    @GetMapping("/getUser")
    public ResponseVo<Object> getUser(Integer id){
        SysUser sysUser = sysUserService.get(id);
        if(sysUser != null) {
            return ResponseVo.success(sysUser);
        }
        return ResponseVo.fail(CodeMsg.NO_CONTENT);
    }

    @ApiOperation(value = "删除单个用户")
    @DeleteMapping("/deleteUser")
    public ResponseVo<Object> deleteUser(Integer id){
        if(sysUserService.delete(id) != 0) {
            return ResponseVo.success("删除用户成功");
        }
        return ResponseVo.fail(CodeMsg.NO_CONTENT);
    }

    @ApiOperation(value = "给用户添加权限")
    @PutMapping("/{id}/{roleId}")
    public ResponseVo<Object> insertSysUser(@PathVariable Integer id, @PathVariable Integer roleId){
        SysUser user = sysUserService.saveRole(id,roleId);
        return ResponseVo.success(user);
    }
}
