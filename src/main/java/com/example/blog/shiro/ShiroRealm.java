
package com.example.blog.shiro;

import com.example.blog.core.model.SysUser;
import com.example.blog.core.service.SysUserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author Created by yanglele on 2019/12/4 10:14
 * @description:自定义的Realm
 */

public class ShiroRealm extends AuthorizingRealm {
    @Autowired
    private SysUserService sysUserService;

     /**
     * @author yanglele 2019/12/4 10:15
     * @description:授权
     */

    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        //给资源进行授权
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        //添加资源的授权字符串
        //info.addStringPermission("user:add");
        //到数据库查询当前登陆用户的授权字符串
        Subject subject = SecurityUtils.getSubject();
        SysUser user = (SysUser)subject.getPrincipal();
        info.addStringPermission(user.getEmail());
        return info;
    }

     /**
     * @author yanglele 2019/12/4 10:15
     * @description:认证（登陆）
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        UsernamePasswordToken token = (UsernamePasswordToken)authenticationToken;
        SysUser user = sysUserService.getSysUserByLoginName(token.getUsername());
        if(user == null) {
            return null;//shiro底层会抛出UnknownAccountException
        }
        return new SimpleAuthenticationInfo("",user.getPassword() ,"");

    }
}

