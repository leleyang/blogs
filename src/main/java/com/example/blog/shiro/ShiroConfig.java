package com.example.blog.shiro;

import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;
import java.util.Map;

@Configuration
public class ShiroConfig {

    //ShiroFilterFactoryBean 过滤工厂对象
    @Bean
    public ShiroFilterFactoryBean getShiroFilterFactoryBean(@Qualifier("getDefaultWebSecurityManager")DefaultWebSecurityManager  securityManager){
        ShiroFilterFactoryBean bean = new ShiroFilterFactoryBean();
        //设置安全管理器
        bean.setSecurityManager(securityManager);
        //添加shiro的内置过滤器
        //anon: 无需认证就可以访问
        //authc : 必须认证了才能访问
        //user: 必须拥有记住我才能访问
        //perms: 拥有对某个资源的权限才能访问
        //role: 拥有某个角色的权限才能访问
        //拦截
        Map<String, String> filterChainDefinitionMap = new LinkedHashMap<>();
        /*filterChainDefinitionMap.put("/", "anon");*/
        /*filterChainDefinitionMap.put("/shop/**", "anon");
        filterChainDefinitionMap.put("/image/**", "anon");
        filterChainDefinitionMap.put("/common/**", "anon");
        filterChainDefinitionMap.put("/bootstrap/**", "anon");
        filterChainDefinitionMap.put("/jquery.min.js", "anon");
        filterChainDefinitionMap.put("/tmallIsv/authorize", "anon");
        filterChainDefinitionMap.put("/img/**", "anon");
        filterChainDefinitionMap.put("/login", "anon");
        filterChainDefinitionMap.put("/unauth", "anon");
        filterChainDefinitionMap.put("/webjars/**", "anon");
        filterChainDefinitionMap.put("/swagger-resources/configuration/ui/**", "anon");
        filterChainDefinitionMap.put("/swagger-resources/configuration/security/**", "anon");
        filterChainDefinitionMap.put("/swagger-ui.html", "anon");
        filterChainDefinitionMap.put("/swagger-resources/**", "anon");
        filterChainDefinitionMap.put("/v2/api-docs/**", "anon");
        filterChainDefinitionMap.put("/webjars/springfox-swagger-ui/**", "anon");*/
        /*filterChainDefinitionMap.put("/**", "anon");*/
        /*filterChainDefinitionMap.put("/add", "perms[user:add]");*/

        bean.setFilterChainDefinitionMap(filterChainDefinitionMap);
        //设置登陆页面
        //bean.setLoginUrl("/login");
        return bean;
    }

    //DefaultWebSecurityManager
    @Bean
    public DefaultWebSecurityManager getDefaultWebSecurityManager(@Qualifier("shiroRealm")ShiroRealm shiroRealm){
        DefaultWebSecurityManager  securityManager = new DefaultWebSecurityManager();
        securityManager.setRealm(shiroRealm);
        return securityManager;
    }


    //创建 realm 对象
    @Bean
    public ShiroRealm shiroRealm(){
        return new ShiroRealm();
    }
}
